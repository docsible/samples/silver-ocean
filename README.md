<!-- DOCSIBLE START -->

# 📃 Role overview

## silver-ocean



Description: Check ocean stuff


| Field                | Value           |
|--------------------- |-----------------|
| Functional description | Not available. |
| Requester            | Not available. |
| Users                | Not available. |
| Date dev             | Not available. |
| Date prod            | Not available. |
| Readme update            | 23/04/2024 |
| Version              | Not available. |
| Time Saving              | Not available. |
| Category              | Not available. |
| Sub category              | Not available. |
| Critical ⚠️            | Not available. |

### Defaults

**These are static variables with lower priority**

#### File: main.yml



| Var          | Type         | Value       |Required    | Title       |
|--------------|--------------|-------------|-------------|-------------|
| [latitude](defaults/main.yml#L6)   | float   | `52.52`  |  True  |  The latitude |
| [longitude](defaults/main.yml#L10)   | float   | `13.41`  |  True  |  The longitude |





### Tasks


#### File: main.yml

| Name | Module | Has Conditions |
| ---- | ------ | --------- |
| Get current date and time | set_fact | False |
| Debug current_time | ansible.builtin.debug | False |
| Fetch hourly weather data from Open-Meteo | ansible.builtin.uri | False |
| Debug print the entire API response | ansible.builtin.debug | False |
| Display temperature for current time and in two hours | ansible.builtin.debug | False |
| Display temperature for two hours ahead | ansible.builtin.debug | False |


## Task Flow Graphs



### Graph for main.yml

```mermaid
flowchart TD
Start
classDef block stroke:#3498db,stroke-width:2px;
classDef task stroke:#4b76bb,stroke-width:2px;
classDef include stroke:#2ecc71,stroke-width:2px;
classDef import stroke:#f39c12,stroke-width:2px;
classDef rescue stroke:#665352,stroke-width:2px;
classDef importPlaybook stroke:#9b59b6,stroke-width:2px;
classDef importTasks stroke:#34495e,stroke-width:2px;
classDef includeTasks stroke:#16a085,stroke-width:2px;
classDef importRole stroke:#699ba7,stroke-width:2px;
classDef includeRole stroke:#2980b9,stroke-width:2px;
classDef includeVars stroke:#8e44ad,stroke-width:2px;

  Start-->|Task| Get_current_date_and_time0[get current date and time]:::task
  Get_current_date_and_time0-->|Task| Debug_current_time1[debug current time]:::task
  Debug_current_time1-->|Task| Fetch_hourly_weather_data_from_Open_Meteo2[fetch hourly weather data from open meteo]:::task
  Fetch_hourly_weather_data_from_Open_Meteo2-->|Task| Debug_print_the_entire_API_response3[debug print the entire api response]:::task
  Debug_print_the_entire_API_response3-->|Task| Display_temperature_for_current_time_and_in_two_hours4[display temperature for current time and in two<br>hours]:::task
  Display_temperature_for_current_time_and_in_two_hours4-->|Task| Display_temperature_for_two_hours_ahead5[display temperature for two hours ahead]:::task
  Display_temperature_for_two_hours_ahead5-->End
```


## Playbook

```yml
---
- hosts: localhost
  connection: local
  roles:
    - role: ../silver-ocean

```
## Playbook graph
```mermaid
flowchart TD
  localhost-->|Role| ___silver_ocean[   silver ocean]
```

## Author Information
Lucian BLETAN

#### License

license (GPL-2.0-or-later, MIT, etc)

#### Minimum Ansible Version

2.1

#### Platforms

- **Fedora**: ['all', 25]

<!-- DOCSIBLE END -->